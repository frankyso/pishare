<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::name('website.')->group(function () {
    Route::get('/auth/login', 'WebController@login')->name('login');
    Route::post('/auth/login', 'WebController@loginPost')->name('login');

    Route::get('/auth/activate/{token}', 'WebController@activateAccount')->name('activate-account');

    Route::get('/auth/register', 'WebController@register')->name('register');
    Route::post('/auth/register', 'WebController@registerPost')->name('register');

    Route::get('/auth/logout', 'WebController@logout')->name('logout');

    Route::get('/', 'WebController@home')->name('home');
    Route::post('upload', 'WebController@upload')->name('upload');

    Route::get('/account', 'WebController@account')->name('account');
    Route::get('/account/images', 'WebController@accountImages')->name('account.images');
    Route::get('/account/images/{picture}/destroy', 'WebController@accountImagesDestory')->name('account.images-destroy');
    Route::get('/account/setting', 'WebController@accountSetting')->name('account.setting');
    Route::post('/account/setting', 'WebController@accountSettingPost')->name('account.setting');

    Route::get('/{slug}', 'WebController@view')->name('view');
    Route::post('/{slug}', 'WebController@viewPost')->name('view');

    Route::get('/page/{slug}', 'WebController@page')->name('page');
});

Route::name('dashboard.')->namespace("Dashboard")->prefix('d')->group(function () {
    Route::get('overview', 'OverviewController@index')->name('overview');

    Route::resource('users', 'UserController')->names([
        'index' => 'users'
    ]);
    Route::resource('images', 'ImageController')->names([
        'index' => 'images'
    ]);
    Route::resource('pages', 'PageController')->names([
        'index' => 'pages'
    ]);
    Route::resource('settings', 'SettingController')->names([
        'index' => 'settings'
    ]);
});