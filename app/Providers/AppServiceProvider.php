<?php

namespace App\Providers;

use App\Page;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer("*", function ($view) {
            $routeName = request()->route()->getName();
            $routeName = str_replace(".", " ", $routeName);
            $routeName = Str::slug("$routeName");
            $view->with('routeName', "page-" . $routeName);

            $pages = Page::get();
            $view->with('navPages', $pages);
        });
    }
}
