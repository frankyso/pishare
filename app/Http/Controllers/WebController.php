<?php

namespace App\Http\Controllers;

use App\Mail\AccountVerification;
use App\Image;
use App\Page;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Auth;
use Image as InterventionImage;
use Mail;
use App;

class WebController extends Controller
{
    public function home()
    {
        return view('frontend.pages.home');
    }

    public function upload(Request $request)
    {
        ini_set('memory_limit', '256M');

        $request->validate(['file' => 'required|image']);

        $picture = new Image;
        $picture->uuid = (string)Str::uuid();
        $picture->name = $request->file('file')->getClientOriginalName();
        $picture->full_path_url = $request->file('file')->store('images/' . date("Y-m-d"), 'public');
        $picture->thumbnail_path_url = "images/" . date("Y-m-d") . "/" . Str::random(40) . ".jpg";

        $image = InterventionImage::make($request->file('file'))->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        $image->save(config('filesystems.disks.public.root') . "/{$picture->thumbnail_path_url}");

        if (Auth::check()) {
            $picture->user_id = Auth::user()->id;
        }

        $picture->save();

        $viewer = url()->route('website.view', ['slug' => $picture->uuid]);
        $fullImage = asset("storage/" . $picture->full_path_url);
        $thumbnailImage = asset("storage/" . $picture->thumbnail_path_url);

        return [
            "viewer" => $viewer,
            "htmlFull" => "<a href=\"$viewer\"><img src=\"$fullImage\" alt=\"{$picture->name}\" border=\"0\"></a>",
            "htmlThumb" => "<a href=\"$viewer\"><img src=\"$thumbnailImage\" alt=\"{$picture->name}\" border=\"0\"></a>",
            "bbcodeFull" => "[url={$viewer}][img]{$fullImage}[/img][/url]",
            "bbcodeThumb" => "[url={$viewer}][img]{$thumbnailImage}[/img][/url]",
        ];
    }

    public function login()
    {
        return view('frontend.pages.login');
    }

    public function loginPost(Request $request)
    {
        $request->validate([
            "email" => "required|email",
            "password" => "required"
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('/');
        } else {
            return redirect()->back()->withInput($request->all())->withErrors(['email' => 'Authentication Failed, Email or Password is not valid']);
        }
    }

    public function register()
    {
        return view('frontend.pages.register');
    }

    public function registerPost(Request $request)
    {
        $request->validate([
            "fullname" => "required",
            "email" => "email|required|unique:users,email",
            "password" => "confirmed|required"
        ]);

        $user = new User;
        $user->name = $request->fullname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        if (!App::environment('demo')) {
            Mail::to($user->email)->send(new AccountVerification($user));
        }

        return view('frontend.pages.register-success')->with('user', $user);
    }

    public function activateAccount($token)
    {
        $id = decrypt($token);
        $user = User::findOrFail($id);
        $user->email_verified_at = now();
        $user->save();

        return redirect()->route('website.login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('website.home');
    }

    public function accountImages()
    {
        $pictures = Auth::user()->images()->paginate();
        return view('frontend.pages.account.image')->with('pictures', $pictures);
    }

    public function accountImagesDestory($picture)
    {
        $picture = Picture::where('uuid', $picture)->firstOrFail();
        $picture->delete();
        return redirect()->back()->with('success', 'Deleted!');
    }

    public function accountSetting()
    {
        return view('frontend.pages.account.setting')->with('user', Auth::user());
    }

    public function accountSettingPost(Request $request)
    {
        $request->validate([
            "fullname" => "required",
            "password" => "confirmed"
        ]);

        $user = Auth::user();
        if ($request->get('password', null) != null) {
            $user->password = bcrypt($request->password);
        }

        $user->name = $request->fullname;
        $user->save();

        return redirect()->back()->with('success', "Account Updated.");
    }

    public function view($slug)
    {
        $picture = Image::where('uuid', $slug)->firstOrFail();

        $viewer = url()->route('website.view', ['slug' => $picture->uuid]);
        $fullImage = asset("storage/" . $picture->full_path_url);
        $thumbnailImage = asset("storage/" . $picture->thumbnail_path_url);
        $embedCode = [
            "viewer" => $viewer,
            "htmlFull" => "<a href=\"$viewer\"><img src=\"$fullImage\" alt=\"{$picture->name}\" border=\"0\"></a>",
            "htmlThumb" => "<a href=\"$viewer\"><img src=\"$thumbnailImage\" alt=\"{$picture->name}\" border=\"0\"></a>",
            "bbcodeFull" => "[url={$viewer}][img]{$fullImage}[/img][/url]",
            "bbcodeThumb" => "[url={$viewer}][img]{$thumbnailImage}[/img][/url]",
        ];

        $owned = $picture->user_id == Auth::user()->id;

        return view('frontend.pages.view')->with('picture', $picture)->with('embedCode', $embedCode)->with('owned', $owned);
    }

    public function viewPost(Request $request, $slug)
    {
        $request->validate(['name' => "required"]);
        $picture = Auth::user()->images()->where('uuid', $slug)->firstOrFail();
        $picture->name = $request->name;
        $picture->save();

        return redirect()->back()->with('success', "Image information updated");

    }

    public function page($slug)
    {
        $page = Page::where('slug', $slug)->firstOrFail();
        return view('frontend.pages.page')->with('page', $page);
    }
}
