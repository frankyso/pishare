<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Image;
use App\User;
use Illuminate\Http\Request;

class OverviewController extends Controller
{
    public function index(){
        $withData['totalImage'] = Image::count();
        $withData['imageInMonth'] = Image::whereMonth('created_at', date("m"))->whereYear("created_at", date("Y"))->count();
        $withData['totalUser'] = User::count();
        $withData['userInMonth'] = User::whereMonth('created_at', date("m"))->whereYear("created_at", date("Y"))->count();

        return view('backend.pages.overview', $withData);
    }
}
