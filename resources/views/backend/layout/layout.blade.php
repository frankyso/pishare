<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}" crossorigin="anonymous">

    <title>{{config('app.name')}} - {{setting('website.tagline')}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="{{$routeName ?? ""}} bg-dark">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="{{route('website.home')}}">{{config('app.name')}} Admin</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item @if(Request::is('d/overview*')) active @endif">
                    <a class="nav-link" href="{{route('dashboard.overview')}}">Overview</a>
                </li>
                <li class="nav-item @if(Request::is('d/users*')) active @endif">
                    <a class="nav-link" href="{{route('dashboard.users')}}">Users</a>
                </li>
                <li class="nav-item @if(Request::is('d/images*')) active @endif">
                    <a class="nav-link" href="{{route('dashboard.images')}}">Images</a>
                </li>
                <li class="nav-item @if(Request::is('d/pages*')) active @endif">
                    <a class="nav-link" href="{{route('dashboard.pages')}}">Pages</a>
                </li>
                <li class="nav-item @if(Request::is('d/settings*')) active @endif">
                    <a class="nav-link" href="{{route('dashboard.settings')}}">Settings</a>
                </li>
                <li class="nav-item dropdown @if(Request::is('account*')) active @endif">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Account
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{route('website.home')}}">Back To Website</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{route('website.logout')}}">Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<div class="container pt-5">
    @yield('content')
</div>

<script src="{{asset('js/app.js')}}" crossorigin="anonymous"></script>
</body>
</html>