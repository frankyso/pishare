@extends('backend.layout.layout')
@section('content')
    <div class="row mb-4">
        <div class="col-md-3">
            <div class="card bg-darker">
                <div class="card-body text-white">
                    <h6 class="text-muted">Total Image</h6>
                    <h2>{{number_format($totalImage)}}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card bg-darker">
                <div class="card-body text-white">
                    <h6 class="text-muted">Image in {{date("F")}}</h6>
                    <h2>{{number_format($imageInMonth)}}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card bg-darker">
                <div class="card-body text-white">
                    <h6 class="text-muted">Total User</h6>
                    <h2>{{number_format($totalUser)}}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card bg-darker">
                <div class="card-body text-white">
                    <h6 class="text-muted">User In {{date("F")}}</h6>
                    <h2>{{number_format($userInMonth)}}</h2>
                </div>
            </div>
        </div>
    </div>
@endsection