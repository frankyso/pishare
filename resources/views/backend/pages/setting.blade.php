@extends('backend.layout.layout')
@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <form action="" method="post" class="text-white-50">
        <div class="card bg-darker">
            <div class="card-body">
                @csrf
                <div class="form-group">
                    <label for="">Website Name</label>
                    <input class="form-control bg-dark border-0 text-white" name="name" value="{{old('name', $websiteName)}}">
                </div>
                <div class="form-group">
                    <label for="">Website Tagline</label>
                    <input class="form-control bg-dark border-0 text-white" name="tagline" value="{{old('tagline', $websiteTagline)}}">
                </div>

                <div class="form-group">
                    <label for="">Ads Script</label>
                    <textarea class="form-control bg-dark border-0 text-white" name="ads">{{old('ads', $websiteAds)}}</textarea>
                </div>

                <div class="form-group">
                    <label for="">Analytics Code</label>
                    <textarea class="form-control bg-dark border-0 text-white" name="analytics">{{old('analytics', $websiteAnalytics)}}</textarea>
                </div>

                <button type="submit" class="btn btn-primary float-right">Save Configuration</button>
            </div>
        </div>
    </form>
@endsection