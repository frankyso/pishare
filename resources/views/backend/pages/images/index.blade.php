@extends('backend.layout.layout')
@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <div class="card-columns">
        @foreach($pictures as $photo)
            <form method="post" action="{{route('dashboard.images.destroy', ['image'=>$photo->id])}}">
                @csrf
                @method('DELETE')
                <div class="card">
                    <img src="{{asset('storage/'.$photo->thumbnail_path_url)}}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title h6">{{$photo->name}}</h5>
                        <a target="_blank" href="{{route('website.view', ['slug'=>$photo->uuid])}}" class="btn btn-primary">View</a>
                        <button type="submit" class="btn btn-outline-danger">Delete</button>
                    </div>
                </div>
            </form>
        @endforeach
    </div>
    <div class="mt-3">{{$pictures->links()}}</div>
@endsection