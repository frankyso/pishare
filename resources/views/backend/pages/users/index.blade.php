@extends('backend.layout.layout')
@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <div class="card-columns">
        @foreach($users as $user)
            <div class="card bg-darker">
                <div class="card-body text-muted">
                    <h3 class="h5 text-white">{{$user->name}}</h3>
                    <p>{{$user->email}}</p>
                    <form class="" method="post" action="{{route('dashboard.users.destroy', ['user'=>$user->id])}}">
                        @csrf
                        @method('DELETE')
                        <div class="">
                            <a href="{{route('dashboard.users.edit', ['user'=>$user->id])}}">
                                <button type="button" class="btn btn-primary">Edit</button>
                            </a>

                            <button type="submit" class="btn btn-outline-danger">Delete</button>
                        </div>
                    </form>
                </div>
            </div>
        @endforeach
    </div>

    <div class="mt-4">
        {{$users->links()}}
    </div>
@endsection