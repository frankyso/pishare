@extends('backend.layout.layout')
@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <div class="row">
        <div class="col-md-6">
            <form action="{{route('dashboard.users.update',["user"=>$user->id])}}" method="post">
                @csrf
                @method('PATCH')
                <div class="card bg-darker text-light">
                    <div class="card-body">
                        <h3 class="mb-4">Edit User</h3>
                        <div class="form-group ">
                            <label for="">Full Name</label>
                            <input type="text" class="form-control @error('fullname') is-invalid @enderror"
                                   name="fullname"
                                   value="{{old('fullname', $user->name)}}">
                            @error("fullname")
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group ">
                            <label for="">Email</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                                   value="{{old('email', $user->email)}}">
                            @error("email")
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary float-right">Update User</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection