@extends('backend.layout.layout')
@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <h1 class="text-white h3 mb-5">Edit Page</h1>

    <form method="post" action="{{route('dashboard.pages.update',['page'=>$page->id])}}">
        @csrf
        @method('PATCH')
        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label for="">Title</label>
                    <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" value="{{old('title', $page->title)}}">
                    @error("title")
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <textarea name="article" cols="30" rows="10" class="summernote">{{old('article', $page->article)}}</textarea>
                @error("article")
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
                <div class="mt-4">
                    <button type="submit" class="btn btn-primary float-right">Save</button>
                </div>
            </div>
        </div>
    </form>
@endsection