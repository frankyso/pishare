@extends('backend.layout.layout')
@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <form method="post" action="{{route('dashboard.pages.store')}}">
        @csrf
        @method('POST')

        <h1 class="text-white h3 mb-5">Add New Page</h1>

        <div class="card">
            <div class="card-body">
                <div class="form-group">
                    <label for="">Title</label>
                    <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" value="{{old('title')}}">
                    @error("title")
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <textarea name="article" cols="30" rows="10" class="summernote">{{old('article')}}</textarea>
                @error("article")
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
                @enderror
                <div class="mt-4">
                    <button type="submit" class="btn btn-primary float-right">Save</button>
                </div>
            </div>
        </div>
    </form>
@endsection