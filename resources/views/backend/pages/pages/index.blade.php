@extends('backend.layout.layout')
@section('content')
    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <div class="card">
        <div class="card-body">
            <a href="{{route('dashboard.pages.create')}}" class="btn btn-primary">Add New Page</a>
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            @foreach($pages as $page)
                <tr>
                    <th width="50" scope="row">{{$page->id}}</th>
                    <td>{{$page->title}}</td>
                    <td width="200" class="text-right">
                        <form method="post" action="{{route('dashboard.pages.destroy', ['page'=>$page->id])}}">
                            @csrf
                            @method('DELETE')
                            <a href="{{route('dashboard.pages.edit', ['page'=>$page->id])}}"
                               class="btn btn-outline-primary btn-sm">Edit </a>
                            <button class="btn btn-outline-danger btn-sm" type="submit">Delete</button>

                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="mt-4">
        {{--        {{$users->links()}}--}}
    </div>
@endsection