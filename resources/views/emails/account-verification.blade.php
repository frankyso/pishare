@component('mail::message')
We received a request to register the {{$user->name}} account at {{ config('app.name') }}.

To complete the process you must activate your account.

@component('mail::button', ['url' => url()->route('website.activate-account', ['token'=>encrypt($user->id)])])
Activate Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent