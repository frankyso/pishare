<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}" crossorigin="anonymous">

    <title>{{config('app.name')}} - {{setting('website.tagline')}}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="{{$routeName ?? ""}}">
<div class="home-cover">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="{{route('website.home')}}">{{config('app.name')}}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            About
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            @foreach($navPages as $page)
                                <a class="dropdown-item" href="{{route('website.page', ['slug'=> $page->slug])}}">{{$page->title}}</a>
                            @endforeach
                        </div>
                    </li>
                </ul>

                <ul class="navbar-nav ml-auto">
                    <li class="nav-item @if(Request::is('/')) active @endif">
                        <a class="nav-link" href="{{route('website.home')}}">Upload <span
                                    class="sr-only">(current)</span></a>
                    </li>

                    @if(Auth::check())
                        <li class="nav-item dropdown @if(Request::is('account*')) active @endif">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Account
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                @if(Auth::user()->role==\App\User::ROLE_ADMINISTRATOR)
                                    <a class="dropdown-item" href="{{route('dashboard.overview')}}">Admin Dashboard</a>
                                @endif
                                <a class="dropdown-item" href="{{route('website.account.images')}}">My Account</a>
                                <a class="dropdown-item" href="{{route('website.account.setting')}}">Setting</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('website.logout')}}">Logout</a>
                            </div>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('website.login')}}">Sign In</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link pt-0 pb-0" href="{{route('website.register')}}">
                                <button class="btn btn-primary">Create Account</button>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        @yield('content')
    </div>
</div>

<script src="{{asset('js/app.js')}}" crossorigin="anonymous"></script>
{!! setting('website.analytics') !!}
</body>
</html>