@extends('frontend.layout.layout')
@section('content')
    <div class="mt-5">
        {!! setting('website.ads') !!}

        <img class="img-fluid rounded" src="{{asset('storage/'.$picture->full_path_url)}}">

        {!! setting('website.ads') !!}
        @if($owned)
            <form method="post" class="mt-3">
                @csrf
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <div class="input-group mb-3">
                    <input type="text" name="name" class="form-control form-control-lg" value="{{$picture->name}}">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="submit" id="button-addon2" >Update</button>
                    </div>
                </div>
            </form>
        @else
            <h3 class="mt-3">{{$picture->name}}</h3>
        @endif
        <div class="border-top mt-4 mb-3"></div>
        <div class="row pb-5">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="text-white">Embed Codes</label>
                    <select class="form-control embed-type">
                        <optgroup label="Links">
                            <option value="{{$embedCode['viewer']}}">Viewer Links</option>
                        </optgroup>
                        <optgroup label="Html Codes">
                            <option value="{{$embedCode['htmlFull']}}">HTML Full Linked</option>
                            <option value="{{$embedCode['htmlThumb']}}">HTML Thumbnail Linked</option>
                        </optgroup>
                        <optgroup label="BBCodes">
                            <option value="{{$embedCode['bbcodeFull']}}">BBCode Full Linked</option>
                            <option value="{{$embedCode['bbcodeThumb']}}">BBCode Thumbnail Linked</option>
                        </optgroup>
                    </select>
                </div>
                <div class="form-group code-result">
                    <textarea rows="3" class="form-control embed-code-textarea" readonly></textarea>
                    <button class="btn btn-dark float-right mt-2" data-clipboard-target=".embed-code-textarea">Copy
                    </button>
                </div>
            </div>
            <div class="col-md-6">
                <div class="js-share"></div>
            </div>
        </div>
    </div>
@endsection