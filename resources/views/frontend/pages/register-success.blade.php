@extends('frontend.layout.layout')
@section('content')
    <div class="mt-5 row">
        <div class="col-md-12 col-lg-12">
            <div class="text-center">
                <h1 class="text-white">{{config('app.name')}} Account Created!</h1>
                <h4 class="text-white">We have send activation link to {{$user->email}}, please check your email to activate your account.</h4>

                <a href="{{route('website.login')}}"><button class="btn btn-primary">Login</button></a>
            </div>
        </div>

    </div>
@endsection