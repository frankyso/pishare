@extends('frontend.layout.layout')
@section('content')
    <div class="mt-5">
        {!! setting('website.ads') !!}
        <h1 class="text-white">Upload and share your images.</h1>
        <h5 class="text-white">Drag and drop anywhere you want and start uploading your images now. 16 MB limit. Direct
            image links, BBCode and HTML thumbnails.</h5>
        <form id="dropzone" class="dropzone mt-5" enctype="multipart/form-data" method="post">
        </form>

        <div class="border-top mt-5 mb-3"></div>
        <div class="form-group">
            <label class="text-white">Embed Codes</label>
            <select class="form-control embed-type">
                <optgroup label="Links">
                    <option value="viewer">Viewer Links</option>
                </optgroup>
                <optgroup label="Html Codes">
                    <option value="htmlFull">HTML Full Linked</option>
                    <option value="htmlThumb">HTML Thumbnail Linked</option>
                </optgroup>
                <optgroup label="BBCodes">
                    <option value="bbcodeFull">BBCode Full Linked</option>
                    <option value="bbcodeThumb">BBCode Thumbnail Linked</option>
                </optgroup>
            </select>
        </div>
        <div class="form-group code-result">
            <textarea rows="8" class="form-control embed-code-textarea" readonly ></textarea>
            <button class="btn btn-dark float-right mt-2" data-clipboard-target=".embed-code-textarea">Copy</button>
        </div>
    </div>
@endsection