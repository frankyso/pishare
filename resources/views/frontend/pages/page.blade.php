@extends('frontend.layout.layout')
@section('content')
    <div class="mt-5">
        <h1 class="h3">{{$page->title}}</h1>
        {!! $page->article !!}
    </div>
@endsection