@extends('frontend.layout.layout')
@section('content')
    <div class="mt-5 row">
        <div class="col-md-8 col-lg-6">
            <div class="text-center">
                <h1 class="text-white">Create {{config('app.name')}} Account</h1>
            </div>

            <form class="mt-5" method="post">
                @csrf
                <div class="form-group text-white">
                    <label>Full Name</label>
                    <input type="text" name="fullname" class="form-control @error('fullname') is-invalid @enderror" value="{{old('fullname')}}">
                    @error("fullname")
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group text-white">
                    <label>Email</label>
                    <input type="text" name="email" class="form-control @error('email') is-invalid @enderror" value="{{old('email')}}">
                    @error("email")
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
                <div class="form-group text-white">
                    <label>Password</label>
                    <input type="password" name="password"
                           class="form-control  @error('password') is-invalid @enderror">
                    @error("password")
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group text-white">
                    <label>Password Confirmation</label>
                    <input type="password" name="password_confirmation"
                           class="form-control  @error('password_confirmation') is-invalid @enderror">
                    @error("password_confirmation")
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary float-right">Register</button>
                </div>
            </form>
        </div>

    </div>
@endsection