@extends('frontend.layout.layout')
@section('content')
    <div class="mt-5 row">
        <div class="col-md-4 col-lg-3 col-xl-3">
            @include('frontend.pages.account._menu')
        </div>
        <div class="col-md-8 col-lg-9">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <div class="card-columns">
                @foreach($pictures as $photo)
                    <div class="card">
                        <img src="{{asset('storage/'.$photo->thumbnail_path_url)}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title h6">{{$photo->name}}</h5>
                            <a href="{{route('website.view', ['slug'=>$photo->uuid])}}" class="btn btn-primary">View /
                                Edit</a>
                            <a href="{{route('website.account.images-destroy', ['picture'=>$photo->uuid])}}" class="btn btn-outline-danger">Delete</a>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="mt-3">{{$pictures->links()}}</div>
        </div>
    </div>
@endsection