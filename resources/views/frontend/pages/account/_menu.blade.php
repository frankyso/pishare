<div class="custom-list-group">
    <a href="{{route('website.account.images')}}" class="list-group-item list-group-item-action @if(Request::is('account/images*')) active @endif">My Uploads</a>
    <a href="{{route('website.account.setting')}}" class="list-group-item list-group-item-action @if(Request::is('account/setting*')) active @endif">Setting</a>
</div>