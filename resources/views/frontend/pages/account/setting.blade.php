@extends('frontend.layout.layout')
@section('content')
    <div class="mt-5 row">
        <div class="col-md-4 col-lg-3 col-xl-3">
            @include('frontend.pages.account._menu')
        </div>
        <div class="col-md-8 col-lg-9">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif

            <form method="post" action="">
                @csrf
                <div class="form-group">
                    <label>Full Name</label>
                    <input class="form-control @error('fullname') is-invalid @enderror" name="fullname" value="{{old('fullname', $user->name)}}">
                    @error("fullname")
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <div class="form-control">{{$user->email}}</div>
                </div>

                <div class="form-group">
                    <label>New Password</label>
                    <input class="form-control @error('password') is-invalid @enderror" name="password" value="">
                    @error("password")
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <div class="form-group">
                    <label>New Password Confirm</label>
                    <input class="form-control" name="password_confirmation" value="">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary float-right">Save Account</button>
                </div>
            </form>
        </div>
    </div>
@endsection