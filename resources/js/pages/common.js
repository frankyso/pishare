/*
 * Copyright (c) Muara Invoasi Bangsa 2019.
 *
 * Every code write on this page is belonging to MIB, don't copy or modify this page without permission from MIB.
 * more information please contact frankyso.mail@gmail.com
 */
export default {
    init() {
        $(".page-website-home, .page-website-login, .page-website-register").fullClip({
            images: [
                'https://images.unsplash.com/flagged/photo-1571501169929-c499133ddb63?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80',
                'https://images.unsplash.com/photo-1571642376444-52558bb8cee2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1926&q=80',
                'https://images.unsplash.com/photo-1571673044258-0340468b7c05?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1989&q=80',
                'https://images.unsplash.com/photo-1571673842290-2bdf61687f1b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80'
            ],
            transitionTime: 4000,
            wait: 15000
        });

        $("form").on("submit", function () {
            $(this).find("[type=submit]").attr("disabled", true);
            $(this).find("[type=submit]").html("<span class=\"spinner-grow spinner-grow-sm\" role=\"status\" aria-hidden=\"true\"></span>\n" +
                "  Loading...\n");
        });

        $(".js-share").jsSocials({
            shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
        });

        $('.summernote').summernote({
            placeholder: 'Start writing your page..',
            tabsize: 2,
            height: 500
        });
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};