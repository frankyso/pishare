export default {
    init() {
        var renderEmbedCodes = function () {
            var embedType = $(".embed-type").val();
            $(".embed-code-textarea").val(embedType);
        }

        $(".embed-type").on("change", function () {
            renderEmbedCodes();
        });

        renderEmbedCodes();
    },
    finalize() {
    },
};