export default {
    init() {
        var embedCodes = {
            bbcodeFull: "",
            bbcodeThumb: "",
            htmlFull: "",
            htmlThumb: "",
            viewer: ""
        }

        var renderEmbedCodes = function () {
            var embedType = $(".embed-type").val();
            $(".embed-code-textarea").val(embedCodes[embedType]);
        }

        $(".embed-type").on("change", function () {
            renderEmbedCodes();
        });

        var mydropzone = $("#dropzone").dropzone({
            maxFiles: 10,
            url: "/upload",
            maxFilesize: 16,
            acceptedFiles: "image/jpeg, image/png, image/gif",
            success: function (file, response) {
                embedCodes.viewer = embedCodes.viewer + response.viewer + "\n";
                embedCodes.htmlFull = embedCodes.htmlFull + response.htmlFull + "\n";
                embedCodes.htmlThumb = embedCodes.htmlThumb + response.htmlThumb + "\n";
                embedCodes.bbcodeThumb = embedCodes.bbcodeThumb + response.bbcodeThumb + "\n";
                embedCodes.bbcodeFull = embedCodes.bbcodeFull + response.bbcodeFull + "\n";

                renderEmbedCodes();
            },
            init: function () {
                // var self = this;
                this.on("sending", function (file, xhr, formData) {
                    formData.append("_token", $('meta[name="csrf-token"]').attr('content'));
                });

                // this.on("complete", function (file) {
                //     self.removeFile(file);
                // });
            }
        });
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};