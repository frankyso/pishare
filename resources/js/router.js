import common from './pages/common';
import pageWebsiteHome from './pages/home';
import pageWebsiteView from './pages/view';
import Router from "./utils/dom-router/Router";

/** Populate Router instance with DOM routes */
const routes = new Router({
    common,
    pageWebsiteHome,
    pageWebsiteView
    // pageWebsitePackage,
});

// Load Events
$(document).ready(() => routes.loadEvents());